package com.example.neo4jdemonstration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Neo4jDemonstrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(Neo4jDemonstrationApplication.class, args);
	}

}
