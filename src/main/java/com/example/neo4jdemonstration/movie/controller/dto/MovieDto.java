package com.example.neo4jdemonstration.movie.controller.dto;

import com.example.neo4jdemonstration.movie.model.entity.MovieNode;
import com.example.neo4jdemonstration.person.model.entity.PersonNode;
import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@ToString(callSuper=true, doNotUseGetters = true)
@Getter
@Setter
public class MovieDto {
    private Long id;
    private String title;
    private String description;
    //private List<Long> actorsAndRoles;
    private List<Long> directors;

    public static MovieNode validateToEntity(MovieDto movieDto, List<PersonNode> directors) {
        //TODO criar a lógica de négocio para validação dos atributos, algumas dessas lógicas podem ser colocadas como anotações nos atributos.
        return MovieNode.builder()
                .id(movieDto.id)
                .title(movieDto.title)
                .description(movieDto.description)
//                .actorsAndRoles(...)
                .directors(directors)
                .build();
    }
}
