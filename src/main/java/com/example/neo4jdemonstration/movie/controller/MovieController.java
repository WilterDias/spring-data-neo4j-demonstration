package com.example.neo4jdemonstration.movie.controller;

import com.example.neo4jdemonstration.movie.controller.dto.MovieDto;
import com.example.neo4jdemonstration.movie.model.service.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/movies")
public class MovieController {

    private final MovieService movieService;

    @PutMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public void update(@RequestBody MovieDto movieDto){
        movieService.update(movieDto);
    }

}
