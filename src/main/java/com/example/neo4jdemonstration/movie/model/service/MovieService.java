package com.example.neo4jdemonstration.movie.model.service;

import com.example.neo4jdemonstration.movie.controller.dto.MovieDto;

import java.util.List;
import java.util.Optional;

public interface MovieService {
    void save(MovieDto movieDto);
    MovieDto get(Integer id);
    MovieDto update(MovieDto movieDto);
    void remove(Integer id);
    List<MovieDto> listAll();
}
