package com.example.neo4jdemonstration.movie.model.repository;

import com.example.neo4jdemonstration.movie.model.entity.MovieNode;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends Neo4jRepository<MovieNode, Long> {
}
