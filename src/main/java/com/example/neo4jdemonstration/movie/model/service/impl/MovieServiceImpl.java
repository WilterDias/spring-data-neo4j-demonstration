package com.example.neo4jdemonstration.movie.model.service.impl;

import com.example.neo4jdemonstration.movie.controller.dto.MovieDto;
import com.example.neo4jdemonstration.movie.model.entity.MovieNode;
import com.example.neo4jdemonstration.movie.model.repository.MovieRepository;
import com.example.neo4jdemonstration.movie.model.service.MovieService;
import com.example.neo4jdemonstration.person.model.entity.PersonNode;
import com.example.neo4jdemonstration.person.model.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;
    private final PersonService personService;

    @Override
    public void save(MovieDto movieDto) {
        //TODO implementar
    }

    @Override
    public MovieDto get(Integer id) {
        //TODO implementar
        return null;
    }

    @Override
    public MovieDto update(MovieDto movieDto) {
        if (!movieRepository.existsById(movieDto.getId())) {
            movieDto.setId(null);
        }
        List<PersonNode> directors = movieDto.getDirectors().stream().map(personService::getNode).collect(Collectors.toList());
        MovieNode movie = movieRepository.save(MovieDto.validateToEntity(movieDto, directors));
        return null;
    }

    @Override
    public void remove(Integer id) {
        //TODO implementar
    }

    @Override
    public List<MovieDto> listAll() {
        //TODO implementar
        return null;
    }
}
