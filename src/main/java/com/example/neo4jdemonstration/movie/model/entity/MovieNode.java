package com.example.neo4jdemonstration.movie.model.entity;

import com.example.neo4jdemonstration.movie.controller.dto.MovieDto;
import com.example.neo4jdemonstration.person.model.entity.PersonNode;
import lombok.*;
import org.springframework.data.neo4j.core.schema.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@ToString(callSuper=true, doNotUseGetters = true)
@Getter
@Setter
@Node("Movie")
public class MovieNode {
    @Id
    @GeneratedValue()
    private Long id;

    private String title;
    @Property("tagline")
    private String description;
    //@Relationship(type = "ACTED_IN", direction = Relationship.Direction.INCOMING)
    //private List<RolesRelationship> actorsAndRoles = new ArrayList<>();
    @Relationship(type = "DIRECTED", direction = Relationship.Direction.INCOMING)
    private List<PersonNode> directors;

    public static MovieDto validateToDto(MovieNode movieNode) {
        //TODO criar a lógica de négocio para validação dos atributos, algumas dessas lógicas podem ser colocadas como anotações nos atributos.
        return MovieDto.builder()
                .id(movieNode.id)
                .title(movieNode.title)
                .description(movieNode.description)
                //.actorsAndRoles(this.actorsAndRoles)
                .directors(movieNode.directors
                        .stream()
                        .map(PersonNode::getId)
                        .collect(Collectors.toList()))
                .build();
    }
}
