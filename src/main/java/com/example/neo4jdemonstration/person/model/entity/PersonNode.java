package com.example.neo4jdemonstration.person.model.entity;

import com.example.neo4jdemonstration.person.controller.dto.PersonDto;
import lombok.*;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@ToString(callSuper=true, doNotUseGetters = true)
@Getter
@Setter
@Node("Person")
public class PersonNode {
    //@RelationshipId
    @Id
    @GeneratedValue()
    private Long id;

    private final String name;

    public static PersonDto validateToDto(PersonNode personNode) {
        //TODO criar a lógica de négocio para validação dos atributos, algumas dessas lógicas podem ser colocadas como anotações nos atributos.
        return PersonDto.builder()
                .id(personNode.id)
                .name(personNode.name)
                .build();
    }

}
