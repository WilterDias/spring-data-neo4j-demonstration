package com.example.neo4jdemonstration.person.model.service;

import com.example.neo4jdemonstration.person.controller.dto.PersonDto;
import com.example.neo4jdemonstration.person.model.entity.PersonNode;

import java.util.List;

public interface PersonService {
    void save(PersonDto personDto);
    PersonDto get(Long id);
    PersonDto update(PersonDto personDto);
    void remove(Integer id);
    List<PersonDto> listAll();

    PersonNode getNode(Long id);
}
