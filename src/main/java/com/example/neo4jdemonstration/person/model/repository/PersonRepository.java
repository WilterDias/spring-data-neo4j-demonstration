package com.example.neo4jdemonstration.person.model.repository;

import com.example.neo4jdemonstration.person.model.entity.PersonNode;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends Neo4jRepository<PersonNode, Long> {
}
