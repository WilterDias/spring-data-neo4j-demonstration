package com.example.neo4jdemonstration.person.model.service.impl;

import com.example.neo4jdemonstration.person.controller.dto.PersonDto;
import com.example.neo4jdemonstration.person.model.entity.PersonNode;
import com.example.neo4jdemonstration.person.model.repository.PersonRepository;
import com.example.neo4jdemonstration.person.model.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {
    private final PersonRepository personRepository;

    @Override
    public void save(PersonDto personDto) {
        //TODO implementar
    }

    @Override
    public PersonDto get(Long id) {
        //TODO implementar
        return null;
    }

    @Override
    public PersonDto update(PersonDto personDto) {
        if (!personRepository.existsById(personDto.getId())) {
            personDto.setId(null);
        }
        return PersonNode.validateToDto(personRepository.save(PersonDto.validateToEntity(personDto)));
    }

    @Override
    public void remove(Integer id) {
        //TODO implementar
    }

    @Override
    public List<PersonDto> listAll() {
        return personRepository.findAll()
                .stream()
                .map(PersonNode::validateToDto)
                .collect(Collectors.toList());
    }

    @Override
    public PersonNode getNode(Long id) {
        return personRepository.findById(id).orElse(null);
    }
}
