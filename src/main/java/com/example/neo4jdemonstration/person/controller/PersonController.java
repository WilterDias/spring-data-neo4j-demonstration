package com.example.neo4jdemonstration.person.controller;

import com.example.neo4jdemonstration.person.controller.dto.PersonDto;
import com.example.neo4jdemonstration.person.model.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/persons")
public class PersonController {
    private final PersonService personService;

    @PutMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public void update(@RequestBody PersonDto personDto){
        personService.update(personDto);
    }

    @GetMapping(value = "/")
    @ResponseStatus(HttpStatus.OK)
    public List<PersonDto> listAll(){
        return personService.listAll();
    }
}
