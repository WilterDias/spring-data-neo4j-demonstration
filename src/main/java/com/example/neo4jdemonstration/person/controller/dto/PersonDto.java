package com.example.neo4jdemonstration.person.controller.dto;

import com.example.neo4jdemonstration.person.model.entity.PersonNode;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@ToString(callSuper=true, doNotUseGetters = true)
@Getter
@Setter
public class PersonDto {
    private Long id;
    private final String name;

    public static PersonNode validateToEntity(PersonDto personDto) {
        //TODO criar a lógica de négocio para validação dos atributos, algumas dessas lógicas podem ser colocadas como anotações nos atributos.
        return PersonNode.builder()
                .id(personDto.id)
                .name(personDto.name)
                .build();
    }
}
