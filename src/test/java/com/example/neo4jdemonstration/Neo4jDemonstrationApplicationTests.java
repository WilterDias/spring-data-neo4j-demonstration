package com.example.neo4jdemonstration;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Neo4jDemonstrationApplicationTests {

	@Disabled
	@Test
	void contextLoads() {
	}

}
