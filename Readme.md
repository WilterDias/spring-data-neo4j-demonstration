# Projeto de exemplo - Spring Boot e Spring Data Neo4j

Esse projeto é visa uma amostra de conhecimento em relação a integrar o Spring Boot com o banco de dados Neo4j.
O Neo4j é um banco de dados voltado a Web Semântica, Grafos de Conhecimento e a Ciência de Dados,
proporcionando análise escalável e de alto desempenho, desenvolvimento de aplicativos inteligentes e 
pipelines avançados de IA/ML. O Cypher é a linguagem de interação e manipulação dos dados no Neo4J.

O intuito da API é cadastrar os filmes e relaciona-los com seus respectivos diretores. Para isso há uma definição
de nó para filme e outra definição de nó para pessoa, havendo uma relação entre pessoas que são diretores com o filme.
Conceitualmente faz jus a definição de tripla no formato RDF (_Resource Description Framework_).

# Instruções a serem consideradas

As informações de instalação do **Docker Engine** se encontra na 
[documentação do Docker](https://docs.docker.com/engine/install/ubuntu/), assim como outras informações, tais como
[executar o Docker sem o Sudo](https://docs.docker.com/engine/install/linux-postinstall/). 
A documentação mais detalhada sobre a instalação da interface gráfica Web para o Docker, 
o **Portainer Community Edition**, se encontra 
na [documentação do Portainer](https://docs.portainer.io/start/install-ce/server/docker/linux). 
A documentação relacionada ao **Spring Data Neo4j** e outras informações sobre o Neo4j se encontram no 
[site do Neo4j](https://neo4j.com/developer/spring-data-neo4j/).
A imagem docker do **Neo4j Community Edition** se 
encontra no [hub do Neo4j](https://hub.docker.com/_/neo4j) e outras informações relacionadas do projeto se encontra
no [github do Neo4j](https://github.com/neo4j/docker-neo4j).
O Postman pode ser baixado no [site do Postman](https://www.postman.com/pricing/), e instalado.
Todas as ferramentas aqui mostradas até o momento permitem o uso comercial sem restrição de uso.
Pode ser vista 
a [licença do Spring Boot](https://github.com/spring-projects/spring-boot/blob/main/LICENSE.txt),
a [licença do Spring Data Neo4j](https://github.com/spring-projects/spring-data-neo4j/blob/main/LICENSE.txt),
a [licença do Docker Engine](https://docs.docker.com/engine/), 
a [licença do Portainer Community Edition](https://github.com/portainer/portainer/blob/develop/LICENSE),
a [licença do Neo4j Community edition](https://github.com/neo4j/neo4j/blob/5.4/LICENSE.txt),
a [licença do Postman](https://support.postman.com/hc/en-us/articles/360003675853-Can-I-use-Postman-for-commercial-use-).

A seguir será mencionado como executar esse projeto.

## Requerimentos
* Java 11+
* Maven
* Postman
* Docker

## Docker
Toda a execução dos comandos do Docker deve ser executada na raiz desse projeto, para não haver erro; 
apesar que é necessário mesmo apenas no build da imagem desse projeto. Para isso copiar ou compartilhar o projeto 
para a pasta da máquina que tenha o Docker instalado.

O Portainer é indispensável, principalmente em times em que há pessoas que podem não saber programar, 
como um Analista de Requisitos, um Product Owner ou um Testador; de modo que não precise
depender sempre dos programadores para cumprir seus trabalhos. Sugere-se utilizar ele também
por ser um facilitador, até mesmo para os programadores.

O WSL pode ser usado para levantar o Docker caso esteja no Windows; no WSL o disco C é montado automaticamente, e
pode ser encontrado em:
```bash
$ cd /mnt/c/
```
A partir de onde está montado o disco C é possível navegar até onde está o projeto nas pastas do Windows.

- **Na raiz de onde está esse projeto**

Execute o comando para instalar o **Portainer** caso não o tenha:
```bash
$ docker volume create portainer_data
$ docker run -d --name portainer -p 8000:8000 -p 9443:9443 --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest
```
Localmente o portainer pode ser acesso pela URL:
<https://localhost:9443/>. Na primeira vez será necessário definir um usuário e senha.

Execute o comando para criar a rede e levantar o container do **Neo4j Community edition**:
```bash
$ docker network create internal-network
$ docker run -d --name neo4j-community --network internal-network -p=7474:7474  -p=7687:7687 --expose 7474 --expose 7687 --volume=$HOME/neo4j/data:/data --volume=$HOME/neo4j/logs:/logs neo4j:community
```
O Gerenciador Web do Neo4j pode ser acessado localmente pela URL:
<http://localhost:7474/browser/>. Na primeira vez será necessário definir o database, usuário e senha; e estes devem ser
o mesmo do que foi configurado no [application.properties](src/main/resources/application.properties)
**<src/main/resources/application.properties>**.

Execute o comando para construir a **imagem desse projeto e levantar o container**:
```bash
$ docker build . --tag neo4j-demonstration:latest --label neo4j
$ docker run -d -p 8080:8080 --expose 8080 --expose 7687 --name neo4j-demonstration --network internal-network -v "$HOME/.m2":/root/.m2 neo4j-demonstration
```
Apague as imagens que foram contruídas anteriormente no Docker que não são mais úteis, para liberar espaço.

Caso tudo esteja certo, poderá ser visto os containers no Portainer da seguinte forma:
![Portainer - containers](img/Portainer%20-%20containers.png)

## Postman
Depois da instalação da instalação do Postman, abrir na ferramenta na opção de importar o arquivo de coleção 
[neo4j-movie.postman_collection.json](postman/neo4j-movie.postman_collection.json)

## Requisições do Postman e Gerenciador Web do Neo4j
Inicialmente é necessário fazer uma ou mais requisições de cadastro de pessoa para a API, para isso será necessário
o uso do Postman; sugere-se no mínimo dois cadastro; usar e modificar no que for conveniente a 
requisições na pasta **person** nomeadas como **save-person-1** e s**ave-person-1**. Depois é necessários listar as 
pessoas que foram cadastradas para saber seus IDs; usar a requisição na pasta **person** nomeada 
como **list-all-persons**.
A seguir a imagem do Postman com a coleção já importada, mostrando a lista de IDs:
 ![Postman - list-all-persons](img/Postman%20-%20requisições.png)

Sabendo os IDs das pessoas é necessário fazer a requisição para cadastrar um filme; usar a requisição 
na pasta **movie** nomeada como **save-movie**, e modificar no Body o atributo
**directors** colocando a lista de IDs referente a pessoas.

Acessando o **Gerenciador Web do Neo4j** por sua URL, com o usuário e senha do banco, será possível ver os nós criados,
e suas relações. Também é possível modificar a label e a cor do nó na sua propriedade do Overview,
como mostrado na imagem a seguir:
![Gerenciador Neo4j - Overview](img/Grafo%20-%20colocar%20a%20cor%20e%20atributo%20que%20define%20o%20node.png)

A deleção de todos os nós pode ser feita pelo seguinte comando no **Gerenciador Web do Neo4j**:
```bash
$ MATCH (n) DETACH DELETE n
```

## Observação
Esse projeto está incompleto, visa apenas ser uma amostra de como funciona o Spring Boot com o Spring Data Neo4j.
