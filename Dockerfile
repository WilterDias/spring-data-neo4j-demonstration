FROM maven:3.8.3-openjdk-17

ADD . /usr/src/example
WORKDIR /usr/src/example
ENTRYPOINT ["mvn","clean","package","exec:java"]